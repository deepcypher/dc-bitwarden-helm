DeepCypher Unofficial Helm Package of BitWarden
===============================================

We wanted to try out BitWarden mainline stack in our Kubernetes clusters.
However there was no first party support for such beyond docker containers.
This is a basic orchestration of BitWarden API and Client.

Instructions to follow.
