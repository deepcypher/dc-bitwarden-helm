CHART_DIR_PATH="charts/dc-bitwarden-helm"
CHART_NAME="dc-bitwarden-helm"
CHART_NAMESPACE="bitwarden"
REGCRED_NAME="regcred"

.PHONY: all
all: lint minikube install

.PHONY: lint
lint: deps
	helm lint ${CHART_DIR_PATH}/.

.PHONY: deps
deps:
	helm dependency update ${CHART_DIR_PATH}/.

.PHONY: minikube
minikube:
	minikube delete
	minikube start
	minikube addons enable ingress

.PHONY: test
test: lint minikube install

.PHONY: template
template: templates.yaml
	helm template --namespace ${CHART_NAMESPACE} --set regcred.secrets[0].name=${REGCRED_NAME} --set namespace.name=${CHART_NAMESPACE} --set namespace.name=${CHART_NAMESPACE} --set namespace.create=true ${CHART_DIR_PATH}/. > templates.yaml

.PHONY: install
install:
	# hack workaround to avoid errors if exist with kubectl create namespace ${CHART_NAMESPACE}
	kubectl create namespace ${CHART_NAMESPACE} --dry-run=client -o yaml | kubectl apply -f -
	helm install --namespace=${CHART_NAMESPACE} --set namespace.name=${CHART_NAMESPACE} --set namespace.create=false ${CHART_NAME} ${CHART_DIR_PATH}/.
	# kubectl get -n ${CHART_NAMESPACE} secret ${REGCRED_NAME} --output="jsonpath={.data.\.dockerconfigjson}" | base64 --decode

.PHONY: upgrade
upgrade:
	helm upgrade --namespace ${CHART_NAMESPACE} ${CHART_NAME} ${CHART_DIR_PATH}/.


.PHONY: uninstall
uninstall:
	helm uninstall --namespace ${CHART_NAMESPACE} ${CHART_NAME}
	kubectl delete namespace ${CHART_NAMESPACE}

.PHONY: perm
perm:
	sudo usermod -aG docker ${USER}

.PHONY: unperm
unperm:
	sudo gpasswd -d ${USER} docker

.PHONY: clean
clean:
	rm -f login.lock login.creds templates.yaml
	minikube delete
